from rest_framework import serializers
from models import Order,Customer,DeliveryBoy,Timeline

class DeliveryBoyLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeliveryBoy
        fields = ('latitude', 'longitude')

class TimelineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Timeline
        fields = ('description','timestamp')