from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse,JsonResponse
from serializers import DeliveryBoyLocationSerializer,TimelineSerializer
from models import DeliveryBoy, Customer, Timeline
# Create your views here.

############# Read-only api ###########################

@csrf_exempt
def getDeliveryGuyLocation(request, id):
    '''

    :param request:
    :param id: order_id
    :return:
    '''

    if request.method == 'GET':
        orderId = id
        deliveryBoy = DeliveryBoy.objects.filter(orderId=orderId)
        deliveryBoyLocationSerializer = DeliveryBoyLocationSerializer(deliveryBoy,many=True)
        print len(deliveryBoy)
        return JsonResponse(deliveryBoyLocationSerializer.data,safe=False)

    return HttpResponse(status=404)

@csrf_exempt
def getDeliveryGuyName(request,id):
    if request.method == 'GET':
        orderId = id
        try:
            deliveryBoy = DeliveryBoy.objects.filter(orderId=orderId)[0]
            return HttpResponse(deliveryBoy.name)
        except DeliveryBoy.DoesNotExist:
            pass
    return HttpResponse("")

@csrf_exempt
def getCustomerLocation(request,id):
    if request.method == 'GET':
        orderId = id
        try:
            customer = Customer.objects.filter(orderId=orderId).order_by('-timestamp')[0]
            location = {'latitude' : customer.latitude,
                        'longitude' : customer.longitude}
            return JsonResponse(location,safe=False)
        except Customer.DoesNotExist:
            pass
    return HttpResponse(status=404)

@csrf_exempt
def getTimeLine(request,id):
    if request.method == 'GET':
        orderId = id
        try:
            timeline = Timeline.objects.filter(orderId=orderId).order_by('timestamp')
            timelineSerializer = TimelineSerializer(timeline,many=True)
            return JsonResponse(timelineSerializer.data,safe=False)
        except Customer.DoesNotExist:
            pass
    return HttpResponse(status=404)


####### Update data api #################

