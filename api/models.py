from __future__ import unicode_literals

from django.db import models

from django.utils import timezone

# Create your models here.


class Order(models.Model):
    uid = models.CharField(max_length=6)
    name = models.CharField(max_length=20)
    timestamp = models.DateTimeField(default=timezone.now())

class Timeline(models.Model):
    uid = models.CharField(max_length=6)
    orderId = models.CharField(max_length=6)
    description = models.TextField()
    timestamp = models.DateTimeField(default=timezone.now())

class Customer(models.Model):
    uid = models.CharField(max_length=6)
    name = models.CharField(max_length=20)
    latitude = models.FloatField()
    longitude = models.FloatField()
    orderId = models.CharField(max_length=6)
    address_string = models.TextField()
    pincode = models.PositiveIntegerField(null=True)
    timestamp = models.DateTimeField(default=timezone.now())

class DeliveryBoy(models.Model):
    uid = models.CharField(max_length=6)
    name = models.CharField(max_length=20)
    latitude = models.FloatField()
    longitude = models.FloatField()
    orderId = models.CharField(max_length=6)
    timelineId = models.CharField(max_length=6)
    timestamp = models.DateTimeField(default=timezone.now())


