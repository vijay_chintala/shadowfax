from django.contrib import admin
from models import Timeline,DeliveryBoy,Customer,Order
# Register your models here.
admin.site.register(Order)
admin.site.register(Customer)
admin.site.register(DeliveryBoy)
admin.site.register(Timeline)